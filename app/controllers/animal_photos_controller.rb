class AnimalPhotosController < ApplicationController
  before_action :authenticate_admin!
  before_action :set_animal_photo, only: [:destroy]

  def create
    @animal_photo = AnimalPhoto.new(animal_photo_params)

    respond_to do |format|
      if @animal_photo.save
        format.json {render json: @animal_photo, status: :ok}
      else
        format.json {render json: @animal_photo.errors.full_messages, status: :unprocessable_entity}
      end
    end
  end
  
  def destroy
    @animal_photo.destroy
    respond_to do |format|
      format.json { head :no_content }
    end
  end

  private
    def animal_photo_params
      params.require(:animal_photo).permit(:animal_id, :content)
    end

    def set_animal_photo
      @animal_photo = AnimalPhoto.find(params[:id])
    end
end
