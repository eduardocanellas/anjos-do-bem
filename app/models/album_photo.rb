class AlbumPhoto < ApplicationRecord
    belongs_to :album

    mount_uploader :content, PhotoUploader

    validates :content, presence: true
end
