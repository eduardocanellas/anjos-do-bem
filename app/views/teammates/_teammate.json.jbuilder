json.extract! teammate, :id, :name, :photo, :nickname, :description, :created_at, :updated_at
json.url teammate_url(teammate, format: :json)
