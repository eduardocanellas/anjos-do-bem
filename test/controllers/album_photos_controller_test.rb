require 'test_helper'

class AlbumPhotosControllerTest < ActionDispatch::IntegrationTest
  test "should get create" do
    get album_photos_create_url
    assert_response :success
  end

  test "should get destroy" do
    get album_photos_destroy_url
    assert_response :success
  end

end
