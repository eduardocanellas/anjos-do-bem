class CreateAnimals < ActiveRecord::Migration[5.2]
  def change
    create_table :animals do |t|
      t.string :name
      t.date :birth
      t.boolean :sex
      t.text :description
      t.text :observations
      t.boolean :adopted
      t.string :owner
      t.string :phone
      t.string :mobile_phone
      t.string :email

      t.timestamps
    end
  end
end
