class CreateTeammates < ActiveRecord::Migration[5.2]
  def change
    create_table :teammates do |t|
      t.string :name
      t.string :photo
      t.string :nickname
      t.text :description

      t.timestamps
    end
  end
end
